Industrial Polymers combines the capabilities of science and technology with a passion for innovating new products. With our onsite polymers chemist and experienced technical staff we have the ability to tap into new markets and opportunities through cooperative product development with our customers. For over 30 years we have practiced the art of invention creating 100's of products for industrial, commercial, municipal, and military needs.

Address: 3250 S Sam Houston Pkwy E, Houston, TX 77047, USA

Phone: 800-766-3832
